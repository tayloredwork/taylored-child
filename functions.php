<?php if ( ! defined( 'ABSPATH' ) ) exit;

add_action( 'wp_enqueue_scripts', 'taylored_child_enqueues', 30 );

function taylored_child_enqueues() {
	// Load Stylesheets
	wp_enqueue_style( 
		'taylored-parent-style', 
		get_template_directory_uri() . '/style.css' 
	);

    wp_enqueue_style( 
		'taylored-child-style', get_stylesheet_uri() 
	);

	// Load JS - comment out if not needed
	wp_enqueue_script( 
		'taylored-general-script', 
		get_stylesheet_directory_uri() . '/js/general.js', 
		array( 'jquery' ) 
	);
}

function hello_taylor_get_lyric() {
	/** These are the lyrics to Hello Dolly */
	$lyrics = "Hello, Dolly
Well, hello, Dolly
It's so nice to have you back where you belong
You're lookin' swell, Dolly
I can tell, Dolly
You're still glowin', you're still crowin'
You're still goin' strong
I feel the room swayin'
While the band's playin'
One of our old favorite songs from way back when
So, take her wrap, fellas
Dolly, never go away again
Hello, Dolly
Well, hello, Dolly
It's so nice to have you back where you belong
You're lookin' swell, Dolly
I can tell, Dolly
You're still glowin', you're still crowin'
You're still goin' strong
I feel the room swayin'
While the band's playin'
One of our old favorite songs from way back when
So, golly, gee, fellas
Have a little faith in me, fellas
Dolly, never go away
Promise, you'll never go away
Dolly'll never go away again";

	// Here we split it into lines
	$lyrics = explode( "\n", $lyrics );

	// And then randomly choose a line
	return wptexturize( $lyrics[ mt_rand( 0, count( $lyrics ) - 1 ) ] );
}

// This just echoes the chosen line, we'll position it later
function hello_taylor() {
	$chosen = hello_taylor_get_lyric();
	echo "<p style='text-align: center;' d='dolly'>$chosen</p>";
}